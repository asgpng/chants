# Release 0.1.1

* Make console output `info` logs by default (#6)
* Make batch mode keep records sorted (#7)
* Don't require CSV inputs to have an ID column (#9)
* Added `extraIndices` argument
* Removed `idIndex` argument
* Removed support for reading from CSV for single-mode matching

# Release 0.1.0

Initial release.
