# Overview

`chants` is a simple utility for identifying matches between Latin chant texts
and verses of Scripture. It uses [fuzzy string
matching](https://en.wikipedia.org/wiki/Approximate_string_matching) to quantify
match quality. More precisely, it uses a similarity ratio based on [Levenshtein
distance](https://en.wikipedia.org/wiki/Levenshtein_distance) to assign each
candidate match a score. Matches with higher scores indicate a greater
likelihood of a true textual reference.

Currently, the only supported Scripture version is the Latin Vulgate from the
[Clementine Project](https://bitbucket.org/clementinetextproject/text.git).

As this is an early release, the API described below should not be considered
stable and could change significantly depending on what is most useful.

# Installation

Pre-built binaries are not directly available from Gitlab yet, but can be
obtained by email.

Additionally, the project can be built from source with prerequisite [Rust
tooling](https://www.rust-lang.org/tools/install) installed and running
```
cargo build --release
```

The corresponding binary will be located at `./target/release/chants`.

# Usage

The utility is self-documenting. For instance, running `chants -h` displays
a description of how to interact with the utility.

A high-level summary of each command is provided below.

## Download

`chants download --directory <directory>` downloads the Latin Vulgate source
from the clementine project to the specified directory. This same directory will
be required as input for future matching invocations.

## Match

`chants match` specifies intent to match chants against the Latin Vulgate
Scipture. Two main modes are supported: matching a single record and
matching a batch of records.

### Single-record matching

A sample single-record match call is below:
```
chants match single -b C:\dev\data\bible\ \
    --text "Propterea unxit te deus deus tuus oleo laetitiae"
```

The output should resemble:
```
[INFO  chants::search] MATCH(91, "Hbr 1:9"):
    CHANT: "Propterea unxit te deus deus tuus oleo laetitiae"
    VERSE: "dilexisti justitiam et odisti iniquitatem propterea unxit te deus deus tuus oleo exsultationis pr participibus tuis"
[INFO  chants::search] MATCH(93, "Ps 44:8"):
    CHANT: "Propterea unxit te deus deus tuus oleo laetitiae"
    VERSE: "dilexisti justitiam et odisti iniquitatem propterea unxit te deus deus tuus oleo l誥iti pr consortibus tuis"
```

Note that non-ASCII characters in the verse have been removed. [A future
enhancement](https://gitlab.com/asgpng/chants/-/issues/5) could improve this by
maintaining a list of common conversions.

### Batch matching

This mode supports reading chant records from an input CSV file and processing
them in parallel.

A sample batch call is below:
```
chants match batch -b C:\dev\data\bible\ \
    -f C:\dev\data\chants\chants.csv \
    --outputPath matches.csv \
    --textIndex 14
    --extraIndices 0 1 2 3
```

* `outputPath` in this case determines the file path to which results should be
written.
* `textIndex` specifies the column index (starting at 0) of the chant text.
* `extraIndices` is an optional parameter which specifies additional columns
    which should be included in the output.

### Common parameters

Both `batch` and `single` match modes take the following parameters, which can
be overridden:

* `minMatchScore`: The minimum score (out of a maximum of 100) that a candidate
    match must score to be included in the result set. The default is 80.
* `numMatches`: The number of matches to include in the result set. The default
    is 5.

## Logging

Additional output is available at runtime by specifying the `RUST_LOG`
environment variable. Valid values are `trace`, `info`, `warn,` `debug`, and
`error`. The default log level is `info`. More detail is available
[here](https://rust-lang-nursery.github.io/rust-cookbook/development_tools/debugging/config_log.html).

# Algorithm

The algorithm uses `partial_ratio`, as described in [Fuzzy String Matching in
Python](https://chairnerd.seatgeek.com/fuzzywuzzy-fuzzy-string-matching-in-python/).
In summary, this method produces a similarity ratio for two strings by first
determining which string is longer, and then computing Levenshtein distance between
the shorter string and substrings of the longer string which have the same length.
The highest ratio among these comparisions is considered the partial ratio.

The actual implementation is in Rust and differs somewhat from the original.
For example, the implementation uses a custom Levenshtein distance method (which
should be very fast), and it also leverages a number of optimizations stemming
from opportunities to short-circuit based on candidate matches which cannot
feasibly yield a full match which exceeds the target `minMatchScore`.

# Performance

Performance of matching should be fairly good with high `minMatchScore` due to
the optimizations which take advantage of short-circuit opportunities (as the
large majority of potential matches will not be high quality). As
`minMatchScore` is reduced, performance will suffer as more candidate matches
must be processed to completion.

To provide some baseline performance expectations, a batch match run on a
low-powered laptop from 2016 (i7-7500U) with 4 logical processors computes
about two matches per second. As batch matching processes matches in parallel,
this number should be expected to increase fairly linearly with core count.
