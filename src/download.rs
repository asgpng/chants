use anyhow::{anyhow, Context, Result};
use clap::ArgMatches;
use git2::Repository;
use log::*;

use std::path::Path;

const GIT_URL: &str = "https://bitbucket.org/clementinetextproject/text.git";

pub fn handle_download_command<'a>(matches: &ArgMatches<'a>) -> Result<()> {
    let directory = matches.value_of("directory").unwrap();
    let path = Path::new(directory);

    if !path.exists() {
        return Err(anyhow!(
            "The specified directory {:?} does not exist",
            directory
        ));
    }

    info!("Cloning repository at {:?}", GIT_URL);

    Repository::clone(GIT_URL, path).with_context(|| format!("Failed to clone repository."))?;

    info!("Clone complete.");

    Ok(())
}
