// External
use anyhow::{anyhow, bail, Context, Result};
use csv::Reader;
use encoding_rs::*;
use log::*;
use std::collections::HashMap;
use std::fs::{read_dir, File};
use std::io::Read;
use std::path::Path;

// Internal
use crate::verse::Verse;

pub fn build_references(path: &Path) -> Result<HashMap<String, Verse>> {
    trace!("Building references from path {:?}", path);
    if !path.is_dir() {
        return Err(anyhow!("Provided argument {:?} was not a directory", path));
    }

    let mut map = HashMap::new();

    let entries = read_dir(path)?
        .map(|res| res.map(|e| e.path()))
        .collect::<Result<Vec<_>, std::io::Error>>()?;

    for entry in entries {
        let entry = entry.as_path();

        // Skip .git directory
        if entry.is_dir() {
            trace!("Skipping directory {:?}", entry);
            continue;
        }

        let file_name = entry.file_name().unwrap();
        let s = file_name
            .to_str()
            .with_context(|| "Could not parse file name as UTF8 string.")?;

        if !s.ends_with(".lat") {
            trace!("Skipping file {:?}", entry);
            continue;
        }

        trace!("Processing {:?}", entry);

        let file_name_tokens: Vec<&str> = s.split(".").collect();
        let file_name = file_name_tokens[0..(file_name_tokens.len() - 1)].join("");

        let bytes = read_iso_utf8(&entry)?;
        let filtered_lines: Vec<u8> = bytes.into_iter().filter(|x| *x != b'\r').collect();

        let lines_iter = filtered_lines.split(|x| *x == b'\n');

        lines_iter.for_each(|x| {
            let z: &[u8] = x;
            let line: String = String::from_utf8(z.to_vec()).unwrap();
            let tokens: Vec<&str> = line.split(' ').collect();
            let reference = String::from(file_name.clone()) + " " + tokens[0];
            let text: String = tokens[1..]
                .join(" ")
                .to_lowercase()
                .chars()
                .filter(|x| x.is_alphabetic() || x.is_whitespace())
                .collect();

            let tokens: Vec<&str> = text.split_whitespace().filter(|x| x.len() > 0).collect();
            let joined = tokens.join(" ");

            let verse = Verse {
                text: joined.clone(),
                text_chars: joined
                    .to_lowercase()
                    .chars()
                    .filter(|x| x.is_alphabetic() || x.is_whitespace())
                    .collect(),
            };

            map.insert(reference, verse);
        });
    }

    Ok(map)
}

pub fn build_chants(
    path: &Path,
    text_index: usize,
    extra_indices: Vec<usize>
) -> Result<Vec<(String, Vec<String>)>> {
    let utf8 = read_iso_utf8(&path)?;
    let utf8_string: String =
        String::from_utf8(utf8).with_context(|| "Unable to read chants file as UTF8")?;

    let mut chants = vec![];
    let mut reader = Reader::from_reader(utf8_string.as_bytes());

    for result in reader.records() {
        let record = result.with_context(|| "Could not parse chant CSV record.")?;

        let text = match record.get(text_index) {
            Some(text) => text,
            None => bail!(format!(
                "No text column (index {}) found for record: {:?}",
                text_index, record
            )),
        };

        let extra_columns = extra_indices.iter().map(|i| {
            record.get(*i)
        }).collect::<Vec<Option<&str>>>();

        let errors = extra_columns.iter().filter(|x| {
             if let Some(_) = x { false } else { true }
        }).count();

        if errors != 0 {
            bail!(format!("One or more errors occurred while parsing columns."));
        }

        let extra_columns = extra_columns.iter().map(|x| {
            String::from(x.unwrap())
        }).collect();
        chants.push((String::from(text), extra_columns));
    }

    Ok(chants)
}

fn read_iso_utf8(path: &Path) -> Result<Vec<u8>> {
    let mut file =
        File::open(&path).with_context(|| format!("Could not read file at {:?}", path))?;
    let mut text = vec![];
    file.read_to_end(&mut text)
        .with_context(|| format!("Could not read file at {:?}", path))?;

    let (cow, _, _) = SHIFT_JIS.decode(&text);
    let (utf8, _, _) = UTF_8.encode(&cow);

    Ok(utf8.to_vec())
}
