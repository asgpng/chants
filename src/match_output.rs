use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct MatchOutput {
    pub score: u8,
    pub reference: String,
    pub reference_text: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct FinalMatchOutput {
    pub record_text: String,
    pub references: String,
}
