pub mod data_reader;
pub mod download;
pub mod levenstein;
pub mod match_output;
pub mod matching;
pub mod ratio;
pub mod search;
pub mod verse;

// external
use clap::clap_app;
use log::error;
use env_logger::{Builder, Env};

// internal
use crate::download::handle_download_command;
use crate::matching::handle_match_command;

fn main() {
    Builder::from_env(Env::default().default_filter_or("info"))
        .init();
    let matches = clap_app!(chants =>
        (version: "0.1.0")
        (author: "Adam Gallagher <asgpng@gmail.com>")
        (about: "Matches chant texts with Latin Bible verses")
        (@subcommand match =>
            (about: "Match texts with Latin Bible verses")
            (@subcommand single =>
                (about: "Match a single chant text with Latin Bible verses")
                (@arg bibleDirectory: -b --bibleDirectory +required +takes_value
                    "The directory of the Bible text files")
                (@arg numMatches: --numMatches -n +takes_value default_value("5")
                    "The number of matches to render")
                (@arg minMatchScore: --minMatchScore +takes_value default_value("80")
                    "The minimum value (out of 100) a match must score to be included in the result set")
                (@arg file: -f --file +takes_value conflicts_with[text]
                    "The name of the chants CSV file")
                (@arg idIndex: --idIndex +takes_value conflicts_with[text]
                    "The index of the ID column in the chants CSV file (first index is 0)")
                (@arg textIndex: --textIndex +takes_value conflicts_with[text]
                    "The index of the text column in the chants CSV file (first index is 0)")
                (@arg id: --id +takes_value conflicts_with[text] "The identifier of the chant")
                (@arg text: --text +takes_value conflicts_with[id] "The chant text")
            )
            (@subcommand batch =>
                (about: "Match a batch of chant texts from a CSV input file")
                (@arg bibleDirectory: -b --bibleDirectory +required +takes_value
                    "The directory of the Bible text files")
                (@arg numMatches: --numMatches -n +takes_value default_value("5")
                    "The number of matches to render")
                (@arg minMatchScore: --minMatchScore +takes_value default_value("80")
                    "The minimum value (out of 100) a match must score to be included in the result set")
                (@arg outputPath: --outputPath +required +takes_value
                    "The file path to which matches should be written")
                (@arg file: -f --file +required +takes_value
                    "The name of the chants CSV file")
                (@arg textIndex: --textIndex +required +takes_value
                    "The index of the text column in the chants CSV file (first index is 0)")
                (@arg extraIndices: --extraIndices +takes_value +multiple
                    "Indices of extra columns from the original CSV file which should be included in the output (first index is 0)")
            )
        )
        (@subcommand download =>
            (about: "Download Latin Bible files.")
            (@arg directory: -d --directory +required +takes_value
                "The target directory to which to download files.")
        )
    )
    .get_matches();

    let result = match matches.subcommand_name() {
        Some("download") => {
            handle_download_command(matches.subcommand_matches("download").unwrap())
        }
        Some("match") => handle_match_command(matches.subcommand_matches("match").unwrap()),
        _ => Ok(()),
    };

    match result {
        Ok(()) => {}
        Err(e) => error!("Encountered an unexpected error: {:?}", e),
    }
}
