// Internal
use crate::match_output::MatchOutput;
use crate::ratio::partial_ratio;
use crate::verse::Verse;

// External
use anyhow::Result;
use csv::Writer;
use log::*;
use rayon::prelude::*;
use std::collections::HashMap;
use std::iter::FromIterator;
use std::path::Path;
use std::string::String;

pub fn batch_mode_search(
    references: &HashMap<String, Verse>,
    chants: &Vec<(String, Vec<String>)>,
    min_match_score: u8,
    num_matches: u8,
    output_path: &Path,
) -> Result<()> {
    info!("Starting batch mode search on {:?} chants", chants.len());

    let matches = chants
        .iter()
        .enumerate()
        .collect::<Vec<_>>();

    let mut matches = matches
        .par_iter()
        .map(|(i, data)| {
            let (chant, other_columns) = data;
            let chant_text = chant.chars().collect();

            let mut inner_matches: Vec<MatchOutput> = references
                .iter()
                .map(|(ref_id, reference)| {
                    match partial_ratio(&chant_text, &reference.text_chars, min_match_score) {
                        Some(score) => {
                            info!(
                                "MATCH({}, {:?}):\n\tCHANT: {:?}\n\tVERSE: {:?}",
                                score,
                                ref_id,
                                String::from_iter(chant_text.clone()),
                                String::from(reference.text.clone()),
                            );
                            let match_output = MatchOutput {
                                score,
                                reference: ref_id.clone(),
                                reference_text: reference.text.clone(),
                            };
                            return Some(match_output);
                        }
                        None => None,
                    }
                })
                .filter(|x| if let Some(_) = x { true } else { false })
                .map(|x| x.unwrap())
                .collect();

            inner_matches.sort_by(|a, b| a.score.cmp(&b.score));

            let final_match =
                inner_matches
                    .iter()
                    .take(num_matches as usize)
                    .fold(vec![], |mut acc, x| {
                        acc.push(x.reference.clone());
                        return acc;
                    });

            let mut all_columns: Vec<String> = other_columns.iter().map(|x| String::from(x)).collect();
            let text = String::from_iter(chant_text.clone());
            let matches = final_match.join(", ");
            all_columns.push(text);
            all_columns.push(matches);
            (i, all_columns)
        })
        .collect::<Vec<_>>();

    matches.sort_by(|a, b| a.0.cmp(b.0));

    let mut writer = Writer::from_path(output_path)?;
    for m in matches {
        let (_, columns) = m;
        writer.serialize(columns)?;
    }

    info!("Finished batch mode search");

    Ok(())
}

pub fn single_reference_search(
    references: &HashMap<String, Verse>,
    text: &str,
    min_match_score: u8,
    num_matches: u8,
) -> Result<()> {
    info!(
        "Searching for top {} matches for chant with Text: {}",
        num_matches, text
    );
    let chant_text = text.chars().collect();
    for (ref_id, reference) in references.iter() {
        match partial_ratio(&chant_text, &reference.text_chars, min_match_score) {
            Some(score) => {
                info!(
                    "MATCH({}, {:?}):\n\tCHANT: {:?}\n\tVERSE: {:?}",
                    score,
                    ref_id,
                    String::from_iter(chant_text.clone()),
                    String::from_iter(reference.text_chars.clone()),
                );
            }
            None => {}
        }
    }

    Ok(())
}
