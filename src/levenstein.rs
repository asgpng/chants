const COST_DELETE: usize = 1;
const COST_INSERT: usize = 1;
const COST_REPLACE: usize = 1;
const COST_EQUALS: usize = 0;

/// Returns the Levenshtein distance between s1 and s2. The min_distance param
/// specifies a threshold distance, which, if exceeded, will cause the execution
/// to short-circuit.
///
/// # Examples
/// let s1 = [1, 2, 3];
/// let s2 = [1, 3];
/// assert_eq!(distance_from(&s1, &s2, 10), Some(1));
pub fn distance_from<T: PartialEq>(s1: &[T], s2: &[T], min_distance: usize) -> Option<usize> {
    // Return early for zero-length inputs
    if s1.len() == 0 {
        return Some(s2.len() * COST_INSERT);
    }

    if s2.len() == 0 {
        return Some(s1.len() * COST_INSERT);
    }

    let (shorter, longer) = if s1.len() <= s2.len() {
        (s1, s2)
    } else {
        (s2, s1)
    };

    // Trim matching prefixes and suffixes
    let prefix_length = get_prefix_length(shorter, longer);
    let suffix_length = get_suffix_length(&shorter[prefix_length..], &longer[prefix_length..]);

    // Re-assign shorter and longer to their unmatched sections
    let (shorter, longer) = (
        &shorter[prefix_length..shorter.len() - suffix_length],
        &longer[prefix_length..longer.len() - suffix_length],
    );

    let mut costs = vec![0; shorter.len() + 1];

    // Initialize first row
    for i in 0..shorter.len() {
        costs[i + 1] = costs[i] + COST_INSERT;
    }

    for i in 0..longer.len() {
        let mut diagonal = costs[0];
        costs[0] = diagonal + COST_DELETE;

        let mut min_local_cost = std::usize::MAX;
        for j in 0..shorter.len() {
            let cost_sub = diagonal
                + if &shorter[j] == &longer[i] {
                    COST_EQUALS
                } else {
                    COST_REPLACE
                };

            let mut cost = cost_sub;
            let cost_del = costs[j + 1] + COST_DELETE;
            cost = if cost_del < cost { cost_del } else { cost };

            let cost_inst = costs[j] + COST_INSERT;
            cost = if cost_inst < cost { cost_inst } else { cost };

            if cost < min_local_cost {
                min_local_cost = cost;
            }

            diagonal = costs[j + 1];
            costs[j + 1] = cost;
        }

        if min_local_cost >= min_distance && shorter.len() > 0 {
            return None;
        }
    }

    Some(costs[shorter.len()])
}

#[inline]
fn get_prefix_length<T: PartialEq>(shorter: &[T], longer: &[T]) -> usize {
    let mut i = 0;
    while i < shorter.len() && &shorter[i] == &longer[i] {
        i += 1;
    }

    i
}

#[inline]
fn get_suffix_length<T: PartialEq>(shorter: &[T], longer: &[T]) -> usize {
    let mut i = 0;
    while i < shorter.len() && &shorter[shorter.len() - i - 1] == &longer[longer.len() - i - 1] {
        i += 1;
    }

    i
}

/// Determine the slices to compare when executing a partial ratio comparison
/// between s1 and s2. The partial ratio comparison involves obtaining the
/// levenstein distance for each substring in the longer slice of length equal
/// to the smaller slice. However, there is no need to compare all such slices
/// if there are matching sub-slices. Instead, we only need to compare the
/// slices starting at matching sub-slices.
pub fn get_partial_blocks<'a, T: PartialEq>(s1: &'a [T], s2: &'a [T]) -> Vec<&'a [T]> {
    // Return early for zero-length inputs
    if s1.len() == 0 {
        return vec![];
    }

    if s2.len() == 0 {
        return vec![];
    }

    let (shorter_orig, longer_orig) = if s1.len() <= s2.len() {
        (s1, s2)
    } else {
        (s2, s1)
    };

    // Trim matching prefixes and suffixes
    let prefix_length = get_prefix_length(shorter_orig, longer_orig);
    let suffix_length = get_suffix_length(
        &shorter_orig[prefix_length..],
        &longer_orig[prefix_length..],
    );

    // Re-assign shorter and longer to their unmatched sections
    let (shorter, longer) = (
        &shorter_orig[prefix_length..shorter_orig.len() - suffix_length],
        &longer_orig[prefix_length..longer_orig.len() - suffix_length],
    );

    // Allocate 1D array to simulate 2D cost matrix
    let mut costs = vec![0; (shorter.len() + 1) * (longer.len() + 1)];

    // Initialize first row
    for i in 0..shorter.len() {
        costs[i + 1] = costs[i] + COST_INSERT;
    }

    let length = shorter.len() + 1;

    // Initialize first column
    for j in 0..longer.len() {
        costs[(j + 1) * length] = costs[j * length] + COST_DELETE;
    }

    // Populate cost matrix
    for i in 0..longer.len() {
        for j in 0..shorter.len() {
            let cost_sub = costs[i * length + j]
                + if &shorter[j] == &longer[i] {
                    COST_EQUALS
                } else {
                    COST_REPLACE
                };

            let mut cost = cost_sub;
            let cost_del = costs[(i + 1) * length + j] + COST_DELETE;
            cost = if cost_del < cost { cost_del } else { cost };

            let cost_ins = costs[i * length + j + 1] + COST_INSERT;
            cost = if cost_ins < cost { cost_ins } else { cost };

            costs[(i + 1) * length + j + 1] = cost;
        }
    }

    // Traverse backwards through and identify matching blocks
    let mut blocks = vec![];

    let mut j = shorter.len();
    let mut i = longer.len();

    let shorter_length = shorter_orig.len();
    let longer_length = longer_orig.len();

    // Add suffix, if necessary
    if suffix_length > 0 {
        blocks.push((
            &longer_orig[longer_length - shorter_length..],
            suffix_length,
        ));
    }

    let mut is_match = false;
    let mut match_length = 0;
    while j != 0 && i != 0 {
        let cost_sub = costs[(i - 1) * length + (j - 1)];
        let is_current_match =
            if cost_sub == costs[i * length + j] && &shorter[j - 1] == &longer[i - 1] {
                true
            } else {
                false
            };

        match (is_current_match, is_match) {
            // Start a new match
            (true, false) => {
                match_length = 1;
                is_match = true;
                j -= 1;
                i -= 1;
            }
            // Continue an existing match
            (true, true) => {
                match_length += 1;
                j -= 1;
                i -= 1;
            }
            // End a match
            (false, true) => {
                is_match = false;
                // blocks.push(&shorter[j..j + match_length]);
                let max = std::cmp::min(j + shorter_length, longer_length);
                let min = max - shorter_length;
                blocks.push((&longer_orig[min..max], match_length));

                // If the current match already reaches the end of the longer
                // slice, no need to process the rest of the blocks
                if max == longer_length {
                    break;
                }

                let cost_del = costs[i * length + (j - 1)];
                let cost_ins = costs[(i - 1) * length + j];

                let mut cost = cost_sub;
                let mut i_n = i - 1;
                let mut j_n = j - 1;

                if cost_del < cost {
                    cost = cost_del;
                    i_n = i;
                    j_n = j - 1;
                }

                if cost_ins < cost {
                    i_n = i - 1;
                    j_n = j;
                }

                i = i_n;
                j = j_n;
            }
            (false, false) => {
                let cost_del = costs[i * length + (j - 1)];
                let cost_ins = costs[(i - 1) * length + j];

                let mut cost = cost_sub;
                let mut i_n = i - 1;
                let mut j_n = j - 1;

                if cost_del < cost {
                    cost = cost_del;
                    i_n = i;
                    j_n = j - 1;
                }

                if cost_ins < cost {
                    i_n = i - 1;
                    j_n = j;
                }

                i = i_n;
                j = j_n;
            }
        }
    }

    if is_match {
        let max = std::cmp::min(j + shorter_length, longer_length);
        let min = max - shorter_length;
        blocks.push((&longer_orig[min..max], match_length));
    }

    // Capture final prefix block, if necessary
    if prefix_length > 0 {
        blocks.push((&longer_orig[0..shorter_length], prefix_length));
    }

    blocks.sort_unstable_by(|a, b| b.1.cmp(&a.1));
    let result: Vec<&[T]> = blocks.iter().map(|x| x.0).collect();
    // result.dedup();

    result
}

#[cfg(test)]
mod tests {
    use crate::levenstein::{distance_from, get_partial_blocks};
    use std::usize::MAX;

    #[test]
    fn distance_of_identical_vectors_is_zero() {
        let v1: Vec<char> = "hello".chars().collect();
        let v2: Vec<char> = "hello".chars().collect();
        assert_eq!(distance_from(&v1, &v2, MAX), Some(0));
    }

    #[test]
    fn distance_of_empty_and_full_is_length_of_full() {
        let v1: Vec<char> = "hello".chars().collect();
        let v2: Vec<char> = "".chars().collect();
        assert_eq!(distance_from(&v1, &v2, MAX), Some(5));
    }

    #[test]
    fn distance_of_vectors_which_differ_by_one_element() {
        let v1: Vec<char> = "hello".chars().collect();
        let v2: Vec<char> = "hallo".chars().collect();
        assert_eq!(distance_from(&v1, &v2, MAX), Some(1));
    }

    #[test]
    fn distance_of_vectors_which_differ_in_length_by_one_character() {
        let v1: Vec<char> = "hello".chars().collect();
        let v2: Vec<char> = "ello".chars().collect();
        assert_eq!(distance_from(&v1, &v2, MAX), Some(1));
    }

    #[test]
    fn partial_blocks_test() {
        let v1: Vec<char> = "kitten".chars().collect();
        let v2: Vec<char> = "sitting".chars().collect();
        let blocks = get_partial_blocks(&v1, &v2);

        assert_eq!(blocks, vec![&v2[1..]]);
    }
}
