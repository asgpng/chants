// External
use anyhow::{anyhow, Result};
use clap::ArgMatches;
use log::info;
use std::path::Path;

// Internal
use crate::data_reader::build_chants;
use crate::data_reader::build_references;
use crate::search::batch_mode_search;
use crate::search::single_reference_search;

pub fn handle_match_command<'a>(matches: &ArgMatches<'a>) -> Result<()> {
    match matches.subcommand_name() {
        Some("single") => {
            handle_single_match_command(matches.subcommand_matches("single").unwrap())
        }
        Some("batch") => handle_batch_match_command(matches.subcommand_matches("batch").unwrap()),
        _ => Ok(()),
    }
}

fn handle_single_match_command<'a>(matches: &ArgMatches<'a>) -> Result<()> {
    let min_match_score = matches.value_of("minMatchScore").unwrap();
    let min_match_score: u8 = min_match_score.parse().unwrap();

    let num_matches = matches.value_of("numMatches").unwrap();
    let num_matches: u8 = num_matches.parse().unwrap();

    let bible_directory = matches.value_of("bibleDirectory").unwrap();
    let bible_path = Path::new(bible_directory);
    if !bible_path.exists() {
        return Err(anyhow!(
            "The specified Bible directory {:?} does not exist",
            bible_path
        ));
    }

    info!("Reading in Bible references");
    let references = build_references(&bible_path)?;

    info!("Number of references: {:?}", references.len());

    if let Some(text) = matches.value_of("text") {
        return single_reference_search(&references, text, min_match_score, num_matches);
    }

    Err(anyhow!("--text is required in single match mode"))
}

fn handle_batch_match_command<'a>(matches: &ArgMatches<'a>) -> Result<()> {
    let min_match_score = matches.value_of("minMatchScore").unwrap();
    let min_match_score: u8 = min_match_score.parse().unwrap();

    let num_matches = matches.value_of("numMatches").unwrap();
    let num_matches: u8 = num_matches.parse().unwrap();

    let bible_directory = matches.value_of("bibleDirectory").unwrap();
    let bible_path = Path::new(bible_directory);
    if !bible_path.exists() {
        return Err(anyhow!(
            "The specified Bible directory {:?} does not exist",
            bible_path
        ));
    }

    info!("Reading in Bible references");
    let references = build_references(&bible_path)?;

    info!("Number of references: {:?}", references.len());

    let output_path = matches.value_of("outputPath").unwrap();
    let output_path = Path::new(output_path);

    let chants_path = matches.value_of("file").unwrap();
    let chants_path = Path::new(chants_path);
    if !chants_path.exists() {
        return Err(anyhow!(
            "The specified chants file {:?} does not exist",
            chants_path
        ));
    }

    let text_index = matches.value_of("textIndex").unwrap();
    let text_index: usize = text_index.parse().unwrap();

    let other_indices = match matches.values_of("extraIndices") {
        Some(i) => i.collect::<Vec<&str>>().iter().map(|x| {
            let index: usize = x.parse().unwrap();
            index
        }).collect(),
        None => vec![]
    };

    let chants = build_chants(&chants_path, text_index, other_indices)?;
    info!("Number of chants: {:?}", chants.len());

    batch_mode_search(
        &references,
        &chants,
        min_match_score,
        num_matches,
        output_path,
    )
}
