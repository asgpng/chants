use crate::levenstein::{distance_from, get_partial_blocks};

/// Determine the similarity ratio of slices s1 and s2 according to their
/// Levenstein distance. `min_match_score` determines the minimum ratio
/// (out of 100) that is being searched for. If the search is found to not
/// possibly yield such a ratio, it terminates early and returns `None`.
/// Otherwise, the corresponding ratio is returned.
pub fn simple_ratio<T: PartialEq + std::fmt::Debug>(
    s1: &Vec<T>,
    s2: &Vec<T>,
    min_match_score: u8,
) -> Option<u8> {
    if s1.len() == 0 || s2.len() == 0 {
        return Some(0);
    }

    let (_, longer) = if s1.len() <= s2.len() {
        (s1, s2)
    } else {
        (s2, s1)
    };

    let length = s1.len() + s2.len();
    let target_distance: usize =
        (length as f32 - length as f32 * min_match_score as f32 / 100.0) as usize;
    let target_distance = std::cmp::min(longer.len(), target_distance);
    match distance_from(s1, s2, target_distance) {
        Some(distance) => {
            let ratio = (length as f32 - distance as f32) / length as f32;
            Some((100.0 * ratio) as u8)
        }
        None => None,
    }
}

/// Determine the best ratio of 'partial' blocks according to the following
/// logic:
/// 1. Fix the shorter slice
/// 2. Determine all eligible sub-slices in the longer slice of the same length
///    as the shorter slice.
/// 3. For efficiency, base (2) on matching sequences.
pub fn partial_ratio<T: PartialEq + std::fmt::Debug>(
    s1: &Vec<T>,
    s2: &Vec<T>,
    min_match_score: u8,
) -> Option<u8> {
    if s1.len() == 0 || s2.len() == 0 {
        if min_match_score > 0 {
            return None;
        } else {
            return Some(0);
        }
    }

    let (shorter, longer) = if s1.len() <= s2.len() {
        (s1, s2)
    } else {
        (s2, s1)
    };

    let shorter_length = shorter.len();
    let total_length = (shorter.len() * 2) as f32;
    let target_distance: usize =
        (total_length as f32 - total_length as f32 * min_match_score as f32 / 100.0) as usize;

    // Check if the distance of the two original strings is small enough to
    // feasibly yield a partial match with the target distance. Here, we
    // use the property that
    // d(shorter, l') + longer.len() - shorter.len() >= d(shorter, longer)
    // for any l' that matches the length of shorter. Thus, if
    // d(shorter, longer) - longer.len() + shorter.len() > target_distance,
    // then by the transitive property, we know that know that
    // d(shorter, l') > target_distance for all l'.
    match distance_from(
        shorter,
        longer,
        target_distance + longer.len() - shorter.len(),
    ) {
        None => {
            return None;
        }
        _ => {}
    }

    let mut min_distance = target_distance;
    let mut has_match_exceeding_threshold = false;

    for block in get_partial_blocks(shorter, longer) {
        debug_assert!(block.len() == shorter_length);
        match distance_from(shorter, block, min_distance) {
            Some(distance) => {
                if distance < min_distance {
                    min_distance = distance;
                    has_match_exceeding_threshold = true;
                }
            }
            None => {}
        }
    }

    if !has_match_exceeding_threshold {
        return None;
    }

    let ratio = (total_length - min_distance as f32) / total_length;
    Some(((ratio * 100.0).round()) as u8)
}

#[cfg(test)]
mod tests {
    use crate::ratio::{partial_ratio, simple_ratio};

    #[test]
    fn simple_ratio_of_identical_vectors_is_100() {
        let v1 = vec![1, 2, 3];
        let v2 = vec![1, 2, 3];
        assert_eq!(simple_ratio(&v1, &v2, 0), Some(100));
    }

    #[test]
    fn simple_ratio_of_empty_vectors_is_0() {
        let v1: Vec<u8> = vec![];
        let v2: Vec<u8> = vec![];
        assert_eq!(simple_ratio(&v1, &v2, 0), Some(0));
    }

    #[test]
    fn simple_ratio_of_close_vectors() {
        let v1: Vec<char> = "Testing FuzzyWuzzy".chars().collect();
        let v2: Vec<char> = "Testing FuzzyWuzzy!!".chars().collect();
        assert_eq!(simple_ratio(&v1, &v2, 0), Some(94));
    }

    #[test]
    fn partial_ratio_of_close_vectors() {
        let v1: Vec<char> = "Testing FuzzyWuzzy".chars().collect();
        let v2: Vec<char> = "Testing FuzzyWuzzy!!".chars().collect();
        assert_eq!(partial_ratio(&v1, &v2, 0), Some(100));
    }

    #[test]
    fn partial_ratio_of_close_vectors_2() {
        let v1: Vec<char> = "Testing FuzzyWuzzy!!".chars().collect();
        let v2: Vec<char> = "Testing FuzzyWuzzy".chars().collect();
        assert_eq!(partial_ratio(&v1, &v2, 0), Some(100));
    }

    #[test]
    fn partial_ratio_chant() {
        let v1: Vec<char> = "qui regis israel inee i eui eu oe oe".chars().collect();
        let v2: Vec<char> = "qui regis isral intende qui deducis velut ovem joseph qui sedes super cherubim manifestare".chars().collect();
        assert_eq!(partial_ratio(&v1, &v2, 0), Some(82));
    }
}
